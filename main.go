package main

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/billing/go-lookup/app"
	"bitbucket.org/billing/go-lookup/handler"
)

func main() {
	for {
		// Initialize Application, Loading Needed Configuration
		err := app.Initialize()
		if err != nil {
			os.Exit(3)
		}

		// Main Flow of Process
		flowControl()

		// Disconnecting From Database
		app.Appl.DB.Close()

		// Sleep Application at The End Of Loop
		app.Sleeping()
	}
}

func flowControl() error {
	var err error

	err = doSplitProcess()
	if err != nil {
		return err
	}

	err = doLookupProcess()
	if err != nil {
		return err
	}

	return nil
}

func doSplitProcess() error {
	var err error

	handlerProcess := handler.Process{}

	fmt.Printf("[ Getting Split Jobs From Database ] : ")
	// Getting Split Jobs From Database
	listProcess, err := handlerProcess.GetProcessSplit()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}

	// Exit Process if jobs is nil or 0
	if listProcess == nil || len(listProcess) <= 0 {
		fmt.Println("No jobs found in database")
		return nil
	}
	fmt.Println(len(listProcess), "Jobs")

	for _, v := range listProcess {
		fmt.Println("[ Processing ] : { Process ID :", v.ProcessID, "}")
		handlerProcess.UpdateStatus(v.ProcessID, "4")

		// Initializing Lookup Struct
		handlerLookup := handler.SplitHandler{
			DataTypeID:  v.DataTypeID,
			ListProcess: &v}

		// Doing Lookup Data Imsi and Tap Code
		handlerLookup.DoSplitMoMt()

		// Close Current Process With Success Type
		handlerProcess.CloseProcessSplit(&handlerLookup.CloseStruct)
	}

	return nil
}

func doLookupProcess() error {
	var err error

	handlerProcess := handler.Process{}

	fmt.Printf("[ Getting Data Structure From Database ] : ")
	// Getting Data Structure From Local Database
	_, err = handlerProcess.GetDataStructure()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")

	// Getting Last Lookup From Local Database
	fmt.Printf("[ Getting Last Lookup From Database ] : ")
	lastLookup, err := handlerProcess.GetLastLookup()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")

	doLookup := false
	timenow := time.Now().Format("20060102150405")

	// If Last Lookup date is empty, then doLookup must be True
	if lastLookup.Lastlookup == "" {
		doLookup = true
	} else {
		// If Last Lookup date is not empty, then compare to current date,
		now, err := time.Parse("20060102150405", timenow)

		then, err := time.Parse("20060102150405", lastLookup.Lastlookup)
		if err != nil {
			fmt.Println(err)
			return nil
		}

		// If current date >= +7 days than Last Lookup date, then doLookup must be True
		if now.Sub(then).Hours()/24 >= float64(app.Appl.Configs.Main.LookupEvery) {
			doLookup = true
		}
	}

	// If LevelDB Imsi not Exist, doLookup must be True
	if _, err := os.Stat(lastLookup.PathImsi); os.IsNotExist(err) {
		doLookup = true
	}

	// If LevelDB Tap Code not Exist, doLookup must be True
	if _, err := os.Stat(lastLookup.PathTapCode); os.IsNotExist(err) {
		doLookup = true
	}

	// If LevelDB Tap Code not Exist, doLookup must be True
	if _, err := os.Stat(lastLookup.PathAnum); os.IsNotExist(err) {
		doLookup = true
	}

	if doLookup == true {
		// Loading Data Imsi & Tap Code From External Database To Local LevelDB
		handlerLookup := handler.LookupHandler{
			PathFileIMSI: "./dataimsi.csv",
			// PathFileIMSI:    "/mnt/datafm/dataimsi.csv",
			PathFileTapCode: "./datatapcode.csv",
			PathDBImsi:      lastLookup.PathImsi,
			PathDBTapCode:   lastLookup.PathTapCode,
			PathDBAnum:      lastLookup.PathAnum,
		}

		err := handlerLookup.LoadImsiTapCode()
		if err != nil {
			return err
		}

		// Updating Last Lookup Date in Local Database
		err = handlerProcess.UpdateLookup(timenow)
		if err != nil {
			return err
		}
	}

	fmt.Printf("[ Getting Partner ID From Database ] : ")
	listPartnerID, err := handlerProcess.GetListPartnerID()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
	}
	fmt.Println("Success")

	fmt.Printf("[ Getting Jobs From Database ] : ")
	// Getting Lookup Jobs From Database
	listProcess, err := handlerProcess.GetProcessLookup()
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}

	// Exit Process if jobs is nil or 0
	if listProcess == nil || len(listProcess) <= 0 {
		fmt.Println("No jobs found in database")
		return nil
	}
	fmt.Println(len(listProcess), "Jobs")

	// Looping through List Process
	for _, v := range listProcess {
		fmt.Println("[ Processing ] : { Process ID :", v.ProcessID, "}")
		handlerProcess.UpdateStatus(v.ProcessID, "4")

		// Initializing Lookup Struct
		handlerLookup := handler.LookupHandler{
			DataTypeID:    v.DataTypeID,
			ListProcess:   &v,
			PathDBImsi:    lastLookup.PathImsi,
			PathDBTapCode: lastLookup.PathTapCode,
			PathDBAnum:    lastLookup.PathAnum,
			ListPartnerID: listPartnerID,
		}

		// Doing Lookup Data Imsi and Tap Code
		handlerLookup.DoLookup()

		// Close Current Process With Success Type
		handlerProcess.CloseProcess(&handlerLookup.CloseStruct)
	}

	return nil
}
