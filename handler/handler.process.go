package handler

import (
	"fmt"

	"bitbucket.org/billing/go-lookup/model"
)

// Process :
type Process struct{}

var modelProcessLookup model.ProcessLookup
var modelProcessSplit model.ProcessSplit

// GetProcessSplit : Handler Funtion For Getting List Of Process Lookup
func (p *Process) GetProcessSplit() ([]model.ProcessSplit, error) {
	listProcess, err := modelProcessSplit.GetProcessSplit()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return listProcess, nil
}

// CloseProcessSplit : Handler Function For Closing Split Process
func (p *Process) CloseProcessSplit(closeStruct *model.ClosingSplit) error {
	fmt.Printf("  > [ Closing Current Process With Status ] : " + closeStruct.ProcessStatusID)
	err := modelProcessLookup.CloseProcessSplit(closeStruct)
	if err != nil {
		fmt.Println(", Failed, with error =>", err.Error())
		return err
	}
	fmt.Println(" => Success")
	return nil
}

// GetProcessLookup : Handler Funtion For Getting List Of Process Lookup
func (p *Process) GetProcessLookup() ([]model.ProcessLookup, error) {
	listProcess, err := modelProcessLookup.GetProcessLookup()
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	return listProcess, nil
}

// UpdateLookup : Handler Function For Updating Last Lookup DateTime
func (p *Process) UpdateLookup(currentLookup string) error {
	err := modelProcessLookup.UpdateLookup(currentLookup)
	if err != nil {
		fmt.Println("  > [ Update Last Lookup Date Failed ] :", err.Error())
		return err
	}
	return nil
}

// UpdateStatus : Handler Function For Updating Process Status
func (p *Process) UpdateStatus(processID, processStatusID string) error {
	err := modelProcessLookup.UpdateStatus(processID, processStatusID)
	if err != nil {
		fmt.Println("  > [ Update Status Failed ] :", err.Error())
		return err
	}
	return nil
}

// GetLastLookup : Handler Function For Get Last Lookup DateTime
func (p *Process) GetLastLookup() (model.LastLookup, error) {
	output, err := modelProcessLookup.GetLastLookup()
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}

// GetListPartnerID :
func (p *Process) GetListPartnerID() (map[string]int, error) {
	output, err := modelProcessLookup.GetListPartnerID()
	if err != nil {
		return output, err
	}

	return output, nil
}

// CloseProcess : Handler Function For Closing Process
func (p *Process) CloseProcess(closeStruct *model.ClosingStruct) error {
	fmt.Printf("  > [ Closing Current Process With Status ] : " + closeStruct.ProcessStatusID)
	err := modelProcessLookup.CloseProcess(closeStruct)
	if err != nil {
		fmt.Println(", Failed, with error =>", err.Error())
		return err
	}
	fmt.Println(" => Success")
	return nil
}

// GetDataStructure : Handler Function For Getting Data Structure
func (p *Process) GetDataStructure() (model.DataStructure, error) {
	output, err := modelProcessLookup.GetDataStructure()
	if err != nil {
		fmt.Println(err.Error())
		return output, err
	}
	return output, nil
}
