package handler

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"bitbucket.org/billing/go-lookup/model"
)

// SplitHandler :
type SplitHandler struct {
	DataTypeID  int
	ListProcess *model.ProcessSplit
	CloseStruct model.ClosingSplit
}

// DoSplitMoMt :
func (p *SplitHandler) DoSplitMoMt() error {
	fmt.Println("  > [ Splitting MO MT ]")

	p.CloseStruct.ProcessConf = p.ListProcess

	// Opening CDR File
	inputCDR := p.ListProcess.PathCdr + p.ListProcess.FilenameCdr
	fmt.Printf("    > [ Opening Input File ] : " + inputCDR + " => ")
	fileCDR, err := os.OpenFile(inputCDR, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return err
	}
	fmt.Println("Success")
	defer fileCDR.Close()

	// File MO intitialization, making MO File directory
	if _, err := os.Stat(p.ListProcess.MoPath); os.IsNotExist(err) {
		fmt.Printf("    > [ Making MO Directory ] : ")
		err = os.MkdirAll(p.ListProcess.MoPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
			return err
		}
		fmt.Println("Success")
	}

	// Creating MO File
	pathMo := p.ListProcess.MoPath + p.ListProcess.MoFilename
	fmt.Printf("      > [ Creating MO File ] : " + pathMo + " => ")
	fileMo, err := os.Create(pathMo)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return err
	}
	fmt.Println("Success")
	defer fileMo.Close()

	// File MO Buffered Writer
	writerMo := bufio.NewWriter(fileMo)

	// File MT intitialization, making MT File directory
	if _, err := os.Stat(p.ListProcess.MtPath); os.IsNotExist(err) {
		fmt.Printf("    > [ Making MT Directory ] : ")
		err = os.MkdirAll(p.ListProcess.MtPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
			return err
		}
		fmt.Println("Success")
	}

	// Creating MT File
	pathMt := p.ListProcess.MtPath + p.ListProcess.MtFilename
	fmt.Printf("      > [ Creating MT File ] : " + pathMt + " => ")
	fileMt, err := os.Create(pathMt)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return err
	}
	fmt.Println("Success")
	defer fileMt.Close()

	// File MT Buffered Writer
	writerMt := bufio.NewWriter(fileMt)

	// File Reject intitialization, making Reject File directory
	if _, err := os.Stat(p.ListProcess.RejectPath); os.IsNotExist(err) {
		fmt.Printf("    > [ Making Reject Directory ] : ")
		err = os.MkdirAll(p.ListProcess.RejectPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
			return err
		}
		fmt.Println("Success")
	}

	// Creating Reject File
	pathReject := p.ListProcess.RejectPath + p.ListProcess.RejectFilename
	fmt.Printf("      > [ Creating Reject File ] : " + pathReject + " => ")
	fileReject, err := os.Create(pathReject)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return err
	}
	fmt.Println("Success")
	defer fileReject.Close()

	// File Reject Buffered Writer
	writerReject := bufio.NewWriter(fileReject)

	// Looping CDR File line by line
	sc := bufio.NewScanner(fileCDR)

	var urutanMoMt int
	// Data Type switch
	switch p.DataTypeID {
	case 1: // OCS Postpaid/Prepaid
		urutanMoMt = 9
	case 2: // TAPIN
		urutanMoMt = 10
	}

	for sc.Scan() {
		if sc.Text() == "" {
			fmt.Fprintln(writerReject, sc.Text())
			p.CloseStruct.TotalReject++
			continue
		}

		p.CloseStruct.TotalInput++

		// Splitting line
		columns := strings.Split(sc.Text(), "|")

		// If columns length < urutanMoMt
		if len(columns) < urutanMoMt+1 {
			fmt.Fprintln(writerReject, sc.Text())
			p.CloseStruct.TotalReject++
			continue
		}

		// If Serivce Filter did not contains MO
		if strings.Contains(columns[urutanMoMt], "MO") {
			fmt.Fprintln(writerMo, sc.Text())
			p.CloseStruct.TotalMo++
		} else if strings.Contains(columns[urutanMoMt], "MT") {
			fmt.Fprintln(writerMt, sc.Text())
			p.CloseStruct.TotalMt++
		} else {
			fmt.Fprintln(writerReject, sc.Text())
			p.CloseStruct.TotalReject++
		}
	}

	// Flushing MO File Buffered Writer
	err = writerMo.Flush()
	if err != nil {
		fmt.Println("    > Error : Unable To Flush File MO Writer, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return err
	}

	// Flushing MT File Buffered Writer
	err = writerMt.Flush()
	if err != nil {
		fmt.Println("    > Error : Unable To Flush File MT Writer, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return err
	}

	// Flushing Reject File Buffered Writer
	err = writerReject.Flush()
	if err != nil {
		fmt.Println("    > Error : Unable To Flush File Reject Writer, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return err
	}

	if p.CloseStruct.TotalInput == 0 {
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", "File Empty"
	} else if p.CloseStruct.TotalReject == p.CloseStruct.TotalReject {
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "7", ""
	} else {
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", "All Records Rejected"
	}

	fmt.Println("    > [ Done ] : { Total Input :", p.CloseStruct.TotalInput, ", MO :", p.CloseStruct.TotalMo,
		", MT :", p.CloseStruct.TotalMt, ", Reject :", p.CloseStruct.TotalReject, "records }")

	return nil
}
