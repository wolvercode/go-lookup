package handler

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"bitbucket.org/billing/go-lookup/app"
	"bitbucket.org/billing/go-lookup/model"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
)

// LookupHandler :
type LookupHandler struct {
	DataTypeID      int
	ListProcess     *model.ProcessLookup
	PathFileIMSI    string
	PathFileTapCode string
	PathDBImsi      string
	PathDBTapCode   string
	PathDBAnum      string
	ConnImsi        *leveldb.DB
	ConnAnum        *leveldb.DB
	ConnTapCode     *leveldb.DB
	mapStartCall    map[int]int
	CloseStruct     model.ClosingStruct
	ListPartnerID   map[string]int
}

var modelLookup model.LookupModel

// LoadImsiTapCode :
func (p *LookupHandler) LoadImsiTapCode() error {
	var err error

	if app.Appl.Configs.Main.LoadFrom == "database" {
		err = modelLookup.DoLoadDataFromDB(p.PathDBImsi, p.PathDBTapCode, p.PathDBAnum)
	} else {
		err = modelLookup.DoLoadFromFile(p.PathDBImsi, p.PathDBTapCode, p.PathDBAnum, p.PathFileIMSI, p.PathFileTapCode)
	}
	return err
}

// DoLookup : Main Handler Function For Lookup IMSI & TAP Code
func (p *LookupHandler) DoLookup() model.ClosingStruct {
	fmt.Println("  > [ Processing Lookup Data Imsi & Tap Code ]")
	var err error

	p.CloseStruct.ProcessConf = p.ListProcess

	// Open Connection to DB IMSI
	fmt.Printf("    > [ Opening DB Imsi ] :" + p.PathDBImsi + " => ")
	p.ConnImsi, err = leveldb.OpenFile(p.PathDBImsi, nil)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return p.CloseStruct
	}
	fmt.Println("Success")

	// Open Connection to DB Tap Code
	fmt.Printf("    > [ Opening DB Tap Code ] :" + p.PathDBTapCode + " => ")
	p.ConnTapCode, err = leveldb.OpenFile(p.PathDBTapCode, nil)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return p.CloseStruct
	}
	fmt.Println("Success")

	// Open Connection to DB Tap Code
	fmt.Printf("    > [ Opening DB A Number ] :" + p.PathDBAnum + " => ")
	p.ConnAnum, err = leveldb.OpenFile(p.PathDBAnum, nil)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return p.CloseStruct
	}
	fmt.Println("Success")

	// Opening CDR File
	inputCDR := p.ListProcess.PathCdr + p.ListProcess.FilenameCdr
	fmt.Printf("    > [ Opening Input File ] : " + inputCDR + " => ")
	fileCDR, err := os.OpenFile(inputCDR, os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return p.CloseStruct
	}
	fmt.Println("Success")
	defer fileCDR.Close()

	// Reject intitialization, making Reject directory
	if _, err := os.Stat(p.ListProcess.RejectPath); os.IsNotExist(err) {
		fmt.Printf("  > [ Making Reject Directory ] : ")
		err = os.MkdirAll(p.ListProcess.RejectPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
			return p.CloseStruct
		}
		fmt.Println("Success")
	}

	// Creating Reject File
	rejectOutput := p.ListProcess.RejectPath + p.ListProcess.RejectFilename
	fmt.Printf("    > [ Creating Reject File ] : " + rejectOutput + " => ")
	fileRejectOutput, err := os.Create(rejectOutput)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return p.CloseStruct
	}
	fmt.Println("Success")
	defer fileRejectOutput.Close()

	// Reject File Buffered Writer
	writerRejectOutput := bufio.NewWriter(fileRejectOutput)

	// Looping CDR File line by line
	sc := bufio.NewScanner(fileCDR)

	p.mapStartCall = make(map[int]int)

	fmt.Println("    > [ Doing Looking Imsi & Tap Code ]")
	// Data Type switch
	switch p.DataTypeID {
	case 1: // OCS Postpaid/Prepaid
		err = p.ForOCS(sc, writerRejectOutput)
	case 2: // TAPIN
		err = p.ForTapin(sc, writerRejectOutput)
	}
	if err != nil {
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return p.CloseStruct
	}

	// Flushing Reject File Buffered Writer
	err = writerRejectOutput.Flush()
	if err != nil {
		fmt.Println("    > Error : Unable To Flush Reject File Writer, with error =>", err.Error())
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", err.Error()
		return p.CloseStruct
	}

	if p.CloseStruct.LookupOutput > 0 {
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "7", ""
		p.sortStartCall()
	} else {
		p.CloseStruct.ProcessStatusID, p.CloseStruct.ErrorMessage = "5", "All Records Rejected"
	}
	p.CloseStruct.LookupInput = (p.CloseStruct.LookupReject + p.CloseStruct.LookupOutput)

	fmt.Println("      > [ Done ] : { Input :", p.CloseStruct.LookupInput, ", Rejected :", p.CloseStruct.LookupReject, ", Output :", p.CloseStruct.LookupOutput, "records }")
	// Closing Connection to DB Imsi
	p.ConnImsi.Close()
	// Closing Connection to DB Tap Code
	p.ConnTapCode.Close()
	// Closing Connection to DB A Number
	p.ConnAnum.Close()

	return p.CloseStruct
}

// CheckPartnerID : Checking Partner ID for MO
func (p *LookupHandler) CheckPartnerID(partnerID string) int {
	if _, ok := p.ListPartnerID[partnerID]; ok {
		return 1
	}

	return 0
}

// ForOCS : Lookup Process for OCS Data Type
func (p *LookupHandler) ForOCS(sc *bufio.Scanner, writerRejectOutput *bufio.Writer) error {
	// Out File intitialization, making Out File directory
	if _, err := os.Stat(p.ListProcess.OutPath); os.IsNotExist(err) {
		fmt.Printf("  > [ Making Out Directory ] : ")
		err = os.MkdirAll(p.ListProcess.OutPath, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	// Creating Out File
	pathOutput := p.ListProcess.OutPath + p.ListProcess.OutFilename
	fmt.Printf("    > [ Creating Out File ] : " + pathOutput + " => ")
	fileOutput, err := os.Create(pathOutput)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")
	defer fileOutput.Close()

	// Out File Buffered Writer
	writerFileOutput := bufio.NewWriter(fileOutput)

	for sc.Scan() {
		// Searching Flag Postpaid/Prepaid by IMSI
		if sc.Text() == "" {
			fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject Field Empty")
			p.CloseStruct.LookupReject++
			p.CloseStruct.RejectFieldEmpty++
			continue
		}

		// Splitting line
		columns := strings.Split(sc.Text(), "|")
		durasi, _ := strconv.Atoi(columns[4])

		if columns[4] == "0" {
			fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject Durasi 0")
			p.CloseStruct.LookupReject++
			p.CloseStruct.RejectDurasi0++
			continue
		}
		// Searching Imsi by A_Num if Imsi field is empty
		if columns[3] == "" || columns[3] == "0" {
			searchByANUM := strings.Replace(columns[1], "+", "", -1) + "-"
			hasil, _ := p.LookImsiByAnum(searchByANUM)
			// If IMSI not found in table, write current line to reject file
			if hasil == "" {
				fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject IMSI")
				p.CloseStruct.LookupReject++
				p.CloseStruct.RejectImsi++
				p.CloseStruct.RejectImsiDurasi += durasi
				continue
			}
			columns[3] = hasil
		}

		// col 10 -> empty || digit
		_, errConv := strconv.Atoi(columns[10])
		if columns[10] == "" || errConv == nil {
			// Searching Tap Code by CellID if Tap Code field is empty
			if columns[11] != "" && len(columns[11]) >= 5 {
				hasil := p.LookTapCode(columns[11][0:5])
				// If Tap Code not found in table, write current line to reject file
				if hasil == "" {
					fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject Tap Code")
					p.CloseStruct.LookupReject++
					p.CloseStruct.RejectTapCode++
					p.CloseStruct.RejectTapCodeDurasi += durasi
					continue
				}

				// Replace column 10 with Tap Code
				columns[10] = hasil
			} else {
				fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject Tap Code")
				p.CloseStruct.LookupReject++
				p.CloseStruct.RejectTapCode++
				p.CloseStruct.RejectTapCodeDurasi += durasi
				continue
			}
		}

		// Checking PartnerID if type MO
		if p.ListProcess.MoMt == "mo" {
			status := p.CheckPartnerID(columns[10])
			if status == 0 {
				fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject MO PartnerID")
				p.CloseStruct.LookupReject++
				p.CloseStruct.RejectMOPartnerID++
				p.CloseStruct.RejectMOPartnerIDDurasi += durasi
				continue
			}
		}

		// Writing New Record to Out File
		recordNew := strings.Join(columns, "|")
		fmt.Fprintln(writerFileOutput, recordNew)
		stcall, _ := strconv.Atoi(columns[0])
		p.mapStartCall[stcall] = 1
		p.CloseStruct.LookupOutput++
	}

	// Flushing Out File Buffered Writer
	err = writerFileOutput.Flush()
	if err != nil {
		fmt.Println("    > Error : Unable To Flush Out File Writer, with error =>", err.Error())
		return err
	}

	return nil
}

// ForTapin : Lookup Process for TAPIN Data Type
func (p *LookupHandler) ForTapin(sc *bufio.Scanner, writerRejectOutput *bufio.Writer) error {
	// Out Tapin Postpaid File intitialization, making File directory
	if _, err := os.Stat(p.ListProcess.OutPathPost); os.IsNotExist(err) {
		fmt.Printf("      > [ Making Out Postpaid Directory ] : ")
		err = os.MkdirAll(p.ListProcess.OutPathPost, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	// Creating Out Tapin Postpaid File
	pathOutPost := p.ListProcess.OutPathPost + p.ListProcess.OutFilenamePost
	fmt.Printf("      > [ Creating Out File ] : " + pathOutPost + " => ")
	fileOutPost, err := os.Create(pathOutPost)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")
	defer fileOutPost.Close()

	// Out Tapin Postpaid File Buffered Writer
	writerOutPost := bufio.NewWriter(fileOutPost)

	// Out Tapin Prepaid File intitialization, making File directory
	if _, err := os.Stat(p.ListProcess.OutPathPre); os.IsNotExist(err) {
		fmt.Printf("      > [ Making Out Prepaid Directory ] : ")
		err = os.MkdirAll(p.ListProcess.OutPathPre, 0777)
		if err != nil {
			fmt.Println("Failed, with error =>", err.Error())
			return err
		}
		fmt.Println("Success")
	}

	// Creating Out Tapin Prepaid File
	pathOutPre := p.ListProcess.OutPathPre + p.ListProcess.OutFilenamePre
	fmt.Printf("      > [ Creating Out File ] : " + pathOutPre + " => ")
	fileOutPre, err := os.Create(pathOutPre)
	if err != nil {
		fmt.Println("Failed, with error =>", err.Error())
		return err
	}
	fmt.Println("Success")
	defer fileOutPre.Close()

	// Out Tapin Prepaid File Buffered Writer
	writerOutPre := bufio.NewWriter(fileOutPre)

	for sc.Scan() {
		// Searching Flag Postpaid/Prepaid by IMSI
		if sc.Text() == "" {
			fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject Field Empty")
			p.CloseStruct.LookupReject++
			p.CloseStruct.RejectFieldEmpty++
			continue
		}

		// Splitting line
		columns := strings.Split(sc.Text(), "|")
		durasi, _ := strconv.Atoi(columns[9])

		// Checking PartnerID if type MO
		if p.ListProcess.MoMt == "mo" {
			status := p.CheckPartnerID(columns[5])
			if status == 0 {
				fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject MO PartnerID")
				p.CloseStruct.LookupReject++
				p.CloseStruct.RejectMOPartnerID++
				p.CloseStruct.RejectMOPartnerIDDurasi += durasi
				continue
			}
		}

		if columns[9] == "0" {
			fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject Durasi 0")
			p.CloseStruct.LookupReject++
			p.CloseStruct.RejectDurasi0++
			continue
		}
		searchByIMSI := columns[1] + "-"
		_, flagPostPre := p.LookAnumByImsi(searchByIMSI)
		// If IMSI not found in table, write current line to reject file
		if flagPostPre == "" {
			fmt.Fprintln(writerRejectOutput, sc.Text(), "-> Reject IMSI")
			p.CloseStruct.LookupReject++
			p.CloseStruct.RejectImsi++
			p.CloseStruct.RejectImsiDurasi += durasi
			continue
		}
		// // Appending Flag Postpaid/Prepaid to last columns
		// columns = append(columns, flagPostPre)

		// Writing New Record to Out File
		recordNew := strings.Join(columns, "|")
		if flagPostPre == "0" {
			fmt.Fprintln(writerOutPost, recordNew)
			p.CloseStruct.TotalTapinPostpaid++
		} else if flagPostPre == "1" {
			fmt.Fprintln(writerOutPre, recordNew)
			p.CloseStruct.TotalTapinPrepaid++
		}

		// Adding StartCall To Map of Startcall
		stcall, _ := strconv.Atoi(columns[6])
		p.mapStartCall[stcall] = 1

		p.CloseStruct.LookupOutput++
	}

	// Flushing Out File Postpaid Buffered Writer
	err = writerOutPost.Flush()
	if err != nil {
		fmt.Println("    > Error : Unable To Flush Out File Writer, with error =>", err.Error())
		return err
	}

	// Flushing Out File Prepaid Buffered Writer
	err = writerOutPre.Flush()
	if err != nil {
		fmt.Println("    > Error : Unable To Flush Out File Writer, with error =>", err.Error())
		return err
	}

	return nil
}

// LookImsiByAnum : Search IMSI By A Number in IMSI LevelDB
func (p *LookupHandler) LookImsiByAnum(inputAnum string) (string, string) {
	var outputImsi string
	var flagPostPre string

	// Searching By Key Prefix
	iter := p.ConnImsi.NewIterator(util.BytesPrefix([]byte(inputAnum)), nil)
	for iter.Next() {
		outputImsi = string(iter.Value())
		flagPostPre = strings.Split(string(iter.Key()), "-")[1]
	}
	iter.Release()
	err := iter.Error()
	if outputImsi == "" || err != nil {
		return "", ""
	}

	return outputImsi, flagPostPre
}

// LookAnumByImsi : Search A Number By IMSI in A Number LevelDB
func (p *LookupHandler) LookAnumByImsi(inputImsi string) (string, string) {
	var outputAnum string
	var flagPostPre string

	// Searching By Key Prefix
	iter := p.ConnAnum.NewIterator(util.BytesPrefix([]byte(inputImsi)), nil)
	for iter.Next() {
		outputAnum = string(iter.Value())
		flagPostPre = strings.Split(string(iter.Key()), "-")[1]
	}
	iter.Release()
	err := iter.Error()
	if outputAnum == "" || err != nil {
		return "", ""
	}

	return outputAnum, flagPostPre
}

// LookTapCode : Search TAP Code in TAP Code LevelDB
func (p *LookupHandler) LookTapCode(inputTapCode string) string {
	var outputTapCode string

	// Searching By Key
	searchTapCode := []byte(inputTapCode)
	hasil, _ := p.ConnTapCode.Get(searchTapCode, nil)
	outputTapCode = string(hasil)

	return outputTapCode
}

func (p *LookupHandler) sortStartCall() {
	// Sorting startcall to get min and max startcall
	var keys []int
	for k := range p.mapStartCall {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	p.CloseStruct.MinStartCall, p.CloseStruct.MaxStartCall = keys[0], keys[len(keys)-1]
}
