package app

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "github.com/lib/pq" // For Postgres Conn
	_ "github.com/mattn/go-oci8"
	"github.com/spf13/viper"
)

// App : Struct of App package
type App struct {
	Configs       *ConfigStruct
	DB            *sql.DB
	DBAPIURL      string
	OraConnString string
}

// Appl : Global variable of App
var Appl App

var ctx = context.Background()

// Initialize : Function for initializing App
func Initialize() error {
	if err := InitConfig(); err != nil {
		return err
	}

	fmt.Println("....Starting " + Appl.Configs.Main.DisplayName + "....")

	switch Appl.Configs.Main.ActiveDB {
	case "postgres":
		if err := ConnPostgres(); err != nil {
			return err
		}
	case "oracle":
		if err := ConnOracle(); err != nil {
			return err
		}
	default:
		err := errors.New("Active Database unrecognized")
		fmt.Println("Unexpected Error :", err.Error())
		return err
	}

	Appl.SetDBAPIURL()

	return nil
}

// Sleeping : Function for sleeping
func Sleeping() {
	fmt.Println("....Sleep for", Appl.Configs.Main.Sleep, "seconds....")
	time.Sleep(time.Duration(Appl.Configs.Main.Sleep) * time.Second)
}

// InitConfig : Function for initializing App Config
func InitConfig() error {
	fmt.Printf("[ Initializing Configuration ] : ")
	viper.AddConfigPath("./")
	viper.SetConfigName("go-lookup")
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Failed, with error :", err.Error())
		return err
	}

	var conf Config
	err := viper.Unmarshal(&conf)
	if err != nil {
		fmt.Println("Failed, with error :", err.Error())
		return err
	}

	switch conf.Active {
	case "dev":
		Appl.Configs = &conf.Dev
	case "prod":
		Appl.Configs = &conf.Prod
	default:
		err = errors.New("Active Config unrecognized")
		fmt.Println("Failed, with error :", err.Error())
		return err
	}

	fmt.Println("Success")

	return nil
}

// ConnPostgres : Function for connecting to PostgreSQL Server
func ConnPostgres() error {
	fmt.Printf("[ Connecting To PostgreSQL Server ] : ")

	confPostgres := Appl.Configs.Postgres

	connectionString := fmt.Sprintf("user=%s password=%s host=%s port=%d dbname=%s sslmode=disable", confPostgres.User, confPostgres.Pass, confPostgres.Host, confPostgres.Port, confPostgres.Db)
	var err error
	Appl.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		fmt.Println("Failed, with error :", err.Error())
		return err
	}

	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()
	if err = Appl.DB.PingContext(ctx); err != nil {
		Appl.DB.Close()
		fmt.Println("Failed, with error :", err.Error())
		return err
	}

	fmt.Println("Success")

	return nil
}

// ConnOracle :
func ConnOracle() error {
	fmt.Printf("[ Connecting To OracleDB Server ] : ")

	dbConf := Appl.Configs.OracleDB

	Appl.OraConnString = fmt.Sprintf("%s/%s@%s:%d/%s", dbConf.User, dbConf.Pass, dbConf.Host, dbConf.Port, dbConf.Db)

	var err error
	Appl.DB, err = sql.Open("oci8", Appl.OraConnString)

	if err = Appl.DB.Ping(); err != nil {
		Appl.DB.Close()
		fmt.Println("Failed, with error :", err.Error())
		return err
	}

	fmt.Println("Success")

	return nil
}

// SetDBAPIURL :
func (a App) SetDBAPIURL() {
	Appl.DBAPIURL = fmt.Sprintf("http://%s:%d/%s/", Appl.Configs.DbAPI.Host, Appl.Configs.DbAPI.Port, Appl.Configs.DbAPI.URL)
}
