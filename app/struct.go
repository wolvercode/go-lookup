package app

// Config : Struct of App Config
type Config struct {
	Main   ConfigStruct
	Active string
	Dev    ConfigStruct
	Prod   ConfigStruct
}

// ConfigStruct : Struct of App Config's Item
type ConfigStruct struct {
	Main     MainStruct
	Postgres DBStruct
	OracleDB DBStruct
	DbAPI    DbAPIStruct
}

// MainStruct : Struct of App Main Config
type MainStruct struct {
	DisplayName string
	Port        int
	Sleep       int
	LookupEvery int
	LoadFrom    string
	ActiveDB    string
}

//DBStruct : Struct of Database Connection
type DBStruct struct {
	Host string
	Port int
	User string
	Pass string
	Db   string
}

// DbAPIStruct : Struct of DB Config
type DbAPIStruct struct {
	Host string
	Port int
	URL  string
}
