set headsep off
set pagesize 0
set wrap off
set feedback off
set newpage 0
set trimspool on
set trimout on
set underline off
set colsep ","
set pages 0
set linesize 9999
set arraysize 5000
set termout off

spool <outputfilename>

SELECT MSISDN || ',' || IMSI || ',' || SUBSCRIBER_TYPE FROM TCNPM608T WHERE ID_STATUS=30 AND IMSI IS NOT NULL;

spool off