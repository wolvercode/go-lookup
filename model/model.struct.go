package model

// DataStructure : Structure Of Data Structure
type DataStructure struct {
	TypeData map[string]DataStructureParameter `json:"data_type"`
}

// DataStructureParameter : Structure Of Data Structure Parameters
type DataStructureParameter struct {
	Parameter map[string]DataStructureProperties `json:"parameter"`
}

// DataStructureProperties : Structure of Data Structure Properties
type DataStructureProperties struct {
	Urutan     int `json:"urutan"`
	SortUrutan int `json:"sort_urutan"`
}

// ProcessSplit :
type ProcessSplit struct {
	ProcessID      string `json:"process_id"`
	ProcesstypeID  string `json:"processtype_id"`
	BatchID        string `json:"batch_id"`
	DataTypeID     int    `json:"data_type_id"`
	Periode        string `json:"periode"`
	DayPeriode     string `json:"day"`
	FlagImsi       int    `json:"flag_imsi"`
	CdrID          int    `json:"cdr_id"`
	PathCdr        string `json:"path_cdr"`
	FilenameCdr    string `json:"filename_cdr"`
	OutID          int    `json:"out_id"`
	MoPath         string `json:"mo_path"`
	MoFilename     string `json:"mo_filename"`
	MtPath         string `json:"mt_path"`
	MtFilename     string `json:"mt_filename"`
	RejectID       int    `json:"reject_id"`
	RejectPath     string `json:"reject_path"`
	RejectFilename string `json:"reject_filename"`
}

// ClosingSplit : Structure of Closing Process Split
type ClosingSplit struct {
	ProcessConf     *ProcessSplit `json:"process_conf"`
	ProcessStatusID string        `json:"processstatus_id"`
	ErrorMessage    string        `json:"error_message"`
	TotalInput      int           `json:"total_input"`
	TotalMo         int           `json:"total_mo"`
	TotalMt         int           `json:"total_mt"`
	TotalReject     int           `json:"total_reject"`
}

// ProcessLookup :
type ProcessLookup struct {
	ProcessID       string `json:"process_id"`
	ProcesstypeID   string `json:"processtype_id"`
	BatchID         string `json:"batch_id"`
	DataTypeID      int    `json:"data_type_id"`
	Periode         string `json:"periode"`
	DayPeriode      string `json:"day"`
	FlagImsi        int    `json:"flag_imsi"`
	MoMt            string `json:"mo_mt"`
	CdrID           int    `json:"cdr_id"`
	PathCdr         string `json:"path_cdr"`
	FilenameCdr     string `json:"filename_cdr"`
	DBImsiID        int    `json:"dbimsi_id"`
	DBImsiPath      string `json:"dbimsi_path"`
	DBTapCodeID     int    `json:"dbtapcode_id"`
	DBTapCodePath   string `json:"dbtapcode_path"`
	OutID           int    `json:"out_id"`
	OutPath         string `json:"out_path"`
	OutFilename     string `json:"out_filename"`
	OutPathPost     string `json:"out_path_post"`
	OutFilenamePost string `json:"out_filename_post"`
	OutPathPre      string `json:"out_path_pre"`
	OutFilenamePre  string `json:"out_filename_pre"`
	RejectID        int    `json:"reject_id"`
	RejectPath      string `json:"reject_path"`
	RejectFilename  string `json:"reject_filename"`
	DBAnumID        int    `json:"dbanum_id"`
	DBAnumPath      string `json:"dbanum_path"`
}

// ClosingStruct : Structure of Closing Process
type ClosingStruct struct {
	ProcessConf             *ProcessLookup `json:"process_conf"`
	ProcessStatusID         string         `json:"processstatus_id"`
	ErrorMessage            string         `json:"error_message"`
	LookupInput             int            `json:"lookup_input"`
	LookupReject            int            `json:"lookup_reject"`
	LookupOutput            int            `json:"lookup_output"`
	MinStartCall            int            `json:"min_startcall"`
	MaxStartCall            int            `json:"max_startcall"`
	RejectFieldEmpty        int            `json:"field_empty"`
	RejectImsi              int            `json:"imsi"`
	RejectNotMo             int            `json:"not_mo"`
	RejectDurasi0           int            `json:"durasi_0"`
	RejectTapCode           int            `json:"tap_code"`
	RejectMOPartnerID       int            `json:"mo_partnerid"`
	TotalTapinPostpaid      int            `json:"total_tapin_postpaid"`
	TotalTapinPrepaid       int            `json:"total_tapin_prepaid"`
	RejectImsiDurasi        int            `json:"imsi_durasi"`
	RejectNotMoDurasi       int            `json:"not_mo_durasi"`
	RejectTapCodeDurasi     int            `json:"tap_code_durasi"`
	RejectMOPartnerIDDurasi int            `json:"mo_partnerid_durasi"`
}

// ListPartnerID :
type ListPartnerID struct {
	PartnerID string `json:"partner_id"`
	Status    int    `json:"status"`
}
