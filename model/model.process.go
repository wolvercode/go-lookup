package model

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"bitbucket.org/billing/go-lookup/app"
)

// LastLookup :
type LastLookup struct {
	PathImsi    string `json:"path_imsi"`
	PathTapCode string `json:"path_tapcode"`
	PathAnum    string `json:"path_anum"`
	Lastlookup  string `json:"last_lookup"`
}

// GetProcessSplit : Model Function For Getting List Process Split From Database
func (p *ProcessSplit) GetProcessSplit() ([]ProcessSplit, error) {
	var listProcess []ProcessSplit

	response, err := http.Get(app.Appl.DBAPIURL + "process/lookup/getProcessSplit")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// CloseProcessSplit : Model Function For Closing Process In Database
func (p *ProcessLookup) CloseProcessSplit(closeStruct *ClosingSplit) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/lookup/closeProcessSplit/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetProcessLookup : Model Function For Getting List Process Lookup From Database
func (p *ProcessLookup) GetProcessLookup() ([]ProcessLookup, error) {
	var listProcess []ProcessLookup

	response, err := http.Get(app.Appl.DBAPIURL + "process/lookup/getProcess")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listProcess)

	return listProcess, nil
}

// GetLastLookup : Model Function For Getting Last Lookup DateTime From Database
func (p *ProcessLookup) GetLastLookup() (LastLookup, error) {
	var output LastLookup

	response, err := http.Get(app.Appl.DBAPIURL + "process/lookup/getLastLookup")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return LastLookup{}, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&output)

	return output, nil
}

// GetListPartnerID :
func (p *ProcessLookup) GetListPartnerID() (map[string]int, error) {
	var listPartnerID []ListPartnerID
	output := make(map[string]int)

	response, err := http.Get(app.Appl.DBAPIURL + "process/lookup/getListPartnerID")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return nil, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&listPartnerID)

	for _, v := range listPartnerID {
		output[v.PartnerID] = v.Status
	}

	if len(output) == 0 {
		return nil, errors.New("No PartnerID found in database")
	}

	return output, nil
}

// UpdateLookup : Model Function For Updating Last Lookup DateTime in Database
func (p *ProcessLookup) UpdateLookup(currentLookup string) error {
	client := &http.Client{}
	response, err := http.NewRequest(http.MethodPut, app.Appl.DBAPIURL+"process/lookup/updateLookup/"+currentLookup, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	_, err = client.Do(response)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// UpdateStatus : Model Function For Updating Process Status in Database
func (p *ProcessLookup) UpdateStatus(processID, processStatusID string) error {
	client := &http.Client{}
	response, err := http.NewRequest(http.MethodPut, app.Appl.DBAPIURL+"process/updateStatus/"+processID+"/"+processStatusID, bytes.NewBuffer(nil))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	_, err = client.Do(response)
	if err != nil {
		// handle error
		log.Fatal(err)
	}
	defer io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}

// GetDataStructure : Model Function For Getting Data Structure From Database
func (p *ProcessLookup) GetDataStructure() (DataStructure, error) {
	var dataStructure DataStructure

	response, err := http.Get(app.Appl.DBAPIURL + "configuration/getDataStructure")
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return dataStructure, err
	}
	defer response.Body.Close()

	defer io.Copy(ioutil.Discard, response.Body)

	json.NewDecoder(response.Body).Decode(&dataStructure)

	return dataStructure, nil
}

// CloseProcess : Model Function For Closing Process In Database
func (p *ProcessLookup) CloseProcess(closeStruct *ClosingStruct) error {
	jsonValue, _ := json.Marshal(closeStruct)
	response, err := http.Post(app.Appl.DBAPIURL+"process/lookup/closeProcess/", "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
		return err
	}
	io.Copy(ioutil.Discard, response.Body)
	defer response.Body.Close()

	return nil
}
