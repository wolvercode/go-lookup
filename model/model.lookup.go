package model

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"bitbucket.org/billing/go-lookup/app"
	"github.com/syndtr/goleveldb/leveldb"
)

// LookupModel :
type LookupModel struct {
	ConnImsi    *leveldb.DB
	ConnAnum    *leveldb.DB
	ConnTapCode *leveldb.DB
}

// TabelImsi : Structure of IMSI Table
type TabelImsi struct {
	ANumber     string `json:"a_number"`
	Imsi        string `json:"imsi"`
	FlagPostPre string `json:"flag_post_pre"`
}

// TabelTapCode : Structure of Tap Code Table
type TabelTapCode struct {
	TapCode     string `json:"tap_code"`
	DigitCellID string `json:"digit_cell_id"`
}

// DoLoadDataFromDB : Main Model Function For Load Data IMSI & Tap Code From Database
func (p *LookupModel) DoLoadDataFromDB(pathDBImsi, pathDBTapCode, PathDBAnum string) error {
	// Loading Data IMSI From Database
	err := p.LoadDataImsiFromDB(pathDBImsi, PathDBAnum)
	if err != nil {
		fmt.Println("Failed : Unable To Load Data Imsi From DB, with error =>", err.Error())

		return err
	}

	// Loading Data Tap Code From Database
	err = p.LoadDataTapCodeFromDB(pathDBTapCode)
	if err != nil {
		fmt.Println("Failed : Unable To Load Data Tap Code From DB, with error =>", err.Error())

		return err
	}

	return nil
}

// DoLoadFromFile : Main Model Function For Load Data IMSI & Tap Code From File
func (p *LookupModel) DoLoadFromFile(pathDBImsi, pathDBTapCode, PathDBAnum, pathFileIMSI, pathFileTapCode string) error {
	// Loading Data IMSI From File
	var err error

	err = p.DumpImsiToCSV(pathFileIMSI)
	if err != nil {
		return err
	}

	// If LevelDB Tap Code not Exist, doLookup must be True
	if _, err := os.Stat(pathFileIMSI); os.IsNotExist(err) {
		fmt.Println("  > Failed :", pathFileIMSI, "does not exists")

		return errors.New("Data Imsi Not Exists")
	}

	err = p.LoadDataImsiFromFile(pathDBImsi, pathFileIMSI, PathDBAnum)
	if err != nil {
		fmt.Println("  > Failed : Unable To Load Data Imsi From File, with error =>", err.Error())

		return err
	}

	// Loading Data Tap Code From File
	err = p.LoadDataTapCodeFromFile(pathDBTapCode, pathFileTapCode)
	if err != nil {
		fmt.Println("  > Failed : Unable To Load Data Tap Code From File, with error =>", err.Error())

		return err
	}

	return nil
}

// DumpImsiToCSV :
func (p *LookupModel) DumpImsiToCSV(pathFileIMSI string) error {
	var err error
	fmt.Println("  > [ Dump Imsi From Database To ] :", pathFileIMSI)

	if _, err = os.Stat(pathFileIMSI); err == nil {
		os.Remove(pathFileIMSI)
	}

	stt, err := ioutil.ReadFile("./dumpimsi.sql")
	if err != nil {
		fmt.Println("    > Failed with error :", err.Error())
		return err
	}
	sqlFl := strings.Replace(string(stt), "<outputfilename>", pathFileIMSI, -1)

	tmpImsiSQL := "./tmpdumpimsi.sql"
	d1 := []byte(sqlFl)
	err = ioutil.WriteFile(tmpImsiSQL, d1, 0644)
	if err != nil {
		fmt.Println("    > Failed with error :", err.Error())
		return err
	}

	inpCommand := "sqlplus -s " + app.Appl.OraConnString + " < " + tmpImsiSQL + " > /dev/null"

	cmd := exec.Command("bash", "-c", inpCommand)

	if err := cmd.Run(); err != nil {
		fmt.Println("    > Failed with error :", err.Error())
		return err
	}

	fmt.Println("    > Success")

	os.Remove(tmpImsiSQL)

	return nil
}

// LoadDataImsiFromFile :
func (p *LookupModel) LoadDataImsiFromFile(pathDBImsi, pathFileIMSI, pathDBAnum string) error {
	fmt.Println("  > [ Making DB IMSI To ] :", pathDBImsi)
	fmt.Println("  > [ Making DB A Number To ] :", pathDBAnum)
	fmt.Println("    > [ From File ] :", pathFileIMSI)

	if _, err := os.Stat(pathDBImsi); err == nil {
		os.RemoveAll(pathDBImsi)
	}

	var err error
	p.ConnImsi, err = leveldb.OpenFile(pathDBImsi, nil)
	if err != nil {
		return err
	}

	p.ConnAnum, err = leveldb.OpenFile(pathDBAnum, nil)
	if err != nil {
		return err
	}

	fileCSV, err := os.OpenFile(pathFileIMSI, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return err
	}
	defer fileCSV.Close()

	sc := bufio.NewScanner(fileCSV)

	countimsi := 0
	countanum := 0
	for sc.Scan() {
		columns := strings.Split(sc.Text(), ",")
		if len(columns) == 0 || len(columns) < 3 {
			continue
		}
		
		if columns[1] == "" {
			continue
		}

		if columns[0][0:1] == "0" {
			columns[0] = "62" + columns[0][1:]
		}

		err = p.ConnImsi.Put([]byte(columns[0]+"-"+columns[2]), []byte(columns[1]), nil)
		if err == nil {
			countimsi++
		}

		err = p.ConnAnum.Put([]byte(columns[1]+"-"+columns[2]), []byte(columns[0]), nil)
		if err == nil {
			countanum++
		}

		fmt.Print(".")
		fmt.Print("\b")
	}

	fmt.Println("    > Success, Total Key Imsi :", countimsi, ", Total Key Anum :", countanum)

	p.ConnImsi.Close()
	p.ConnAnum.Close()

	return nil
}

// LoadDataTapCodeFromFile :
func (p *LookupModel) LoadDataTapCodeFromFile(pathDBTapCode, pathFileTapCode string) error {
	var err error

	fmt.Println("  > [ Making DB Tap Code To ] :", pathDBTapCode)
	fmt.Println("    > [ From File ] :", pathFileTapCode)

	if _, err = os.Stat(pathDBTapCode); err == nil {
		os.RemoveAll(pathDBTapCode)
	}

	p.ConnTapCode, err = leveldb.OpenFile(pathDBTapCode, nil)
	if err != nil {
		return err
	}

	fileCSV, err := os.OpenFile(pathFileTapCode, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return err
	}
	defer fileCSV.Close()

	sc := bufio.NewScanner(fileCSV)

	count := 0
	for sc.Scan() {
		columns := strings.Split(sc.Text(), ",")
		err = p.ConnTapCode.Put([]byte(columns[2]), []byte(columns[1]), nil)
		if err == nil {
			count++
		}
		fmt.Print(".")
		fmt.Print("\b")
	}

	fmt.Println("    > Success, Total Key :", count)

	p.ConnTapCode.Close()

	return nil
}

// LoadDataImsiFromDB : Model Function For Loading Data IMSI From Database
func (p *LookupModel) LoadDataImsiFromDB(pathDBImsi, pathDBAnum string) error {
	fmt.Println("  > [ Making DB IMSI To ] :", pathDBImsi)
	fmt.Println("  > [ Making DB A Number To ] :", pathDBAnum)
	applDB := app.Appl.DB

	if _, err := os.Stat(pathDBImsi); err == nil {
		os.RemoveAll(pathDBImsi)
	}

	if _, err := os.Stat(pathDBAnum); err == nil {
		os.RemoveAll(pathDBAnum)
	}

	var err error
	p.ConnImsi, err = leveldb.OpenFile(pathDBImsi, nil)
	if err != nil {
		return err
	}

	p.ConnAnum, err = leveldb.OpenFile(pathDBAnum, nil)
	if err != nil {
		return err
	}

	query := "SELECT a_number, imsi, flag_post_pre FROM roaming_vc_schema.vc_t_imsi WHERE status='ACTIVE'"
	rows, err := applDB.Query(query)
	if err != nil {
		return err
	}

	countimsi := 0
	countanum := 0
	for rows.Next() {
		scanProcess := TabelImsi{}
		rows.Scan(&scanProcess.ANumber, &scanProcess.Imsi, &scanProcess.FlagPostPre)
		err = p.ConnImsi.Put([]byte(scanProcess.ANumber+"-"+scanProcess.FlagPostPre), []byte(scanProcess.Imsi), nil)
		if err == nil {
			countimsi++
		}
		err = p.ConnAnum.Put([]byte(scanProcess.Imsi+"-"+scanProcess.FlagPostPre), []byte(scanProcess.ANumber), nil)
		if err == nil {
			countanum++
		}
	}

	fmt.Println("    > Success, Total Key Imsi :", countimsi, ", Total Key Anum :", countanum)

	p.ConnAnum.Close()
	p.ConnImsi.Close()

	return nil
}

// LoadDataTapCodeFromDB : Model Function For Loading Data Tap Code From Database
func (p *LookupModel) LoadDataTapCodeFromDB(pathDBTapCode string) error {
	var err error

	fmt.Println("  > [ Making DB Tap Code To ] :", pathDBTapCode)
	applDB := app.Appl.DB

	if _, err = os.Stat(pathDBTapCode); err == nil {
		os.RemoveAll(pathDBTapCode)
	}

	p.ConnTapCode, err = leveldb.OpenFile(pathDBTapCode, nil)
	if err != nil {
		return err
	}

	query := "SELECT tapcode, digitcellid FROM roaming_vc_schema.vc_t_tapcode"
	rows, err := applDB.Query(query)
	if err != nil {
		return err
	}

	counts := 0
	for rows.Next() {
		scanProcess := TabelTapCode{}
		rows.Scan(&scanProcess.TapCode, &scanProcess.DigitCellID)
		err = p.ConnTapCode.Put([]byte(scanProcess.DigitCellID), []byte(scanProcess.TapCode), nil)
		if err == nil {
			counts++
		}
	}

	fmt.Println("    > Success, Total Key :", counts)

	p.ConnTapCode.Close()

	return nil
}
