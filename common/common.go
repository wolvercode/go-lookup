package common

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

// RespondWithJSON : Gives JSON Response on Request
func RespondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// RespondWithError : Gives JSON Response with Error message on Request
func RespondWithError(w http.ResponseWriter, code int, message string) {
	RespondWithJSON(w, code, map[string]string{"error": message})
}

// Encrypt : encrypt string to base64 crypto using AES
func Encrypt(text string) string {
	// key := []byte(keyText)
	key := []byte("a-32-bytes-keys!")
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext)
}

// Decrypt : decrypt from base64 to decrypted string
func Decrypt(cryptoText string) string {
	key := []byte("a-32-bytes-keys!")
	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	// The IV needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(ciphertext, ciphertext)

	return fmt.Sprintf("%s", ciphertext)
}

// AddingUTCTime : Adding UTC Time to Date, eg : AddingUTCTime("20060102150405", "20180925161820", "+0800")
func AddingUTCTime(inpFormat, inpDatetime, utcTime string) (string, error) {
	t, err := time.Parse(inpFormat, inpDatetime)
	if err != nil {
		return "", err
	}
	thour, err := time.Parse("1504", strings.Replace(utcTime, "+", "", -1))
	if err != nil {
		return "", err
	}
	offset := time.Duration(thour.Hour())*time.Hour + time.Duration(thour.Minute())*time.Minute

	newDateUTC := t.UTC().Add(offset).Format(inpFormat)

	return newDateUTC, nil
}

// ParseDatetime :
func ParseDatetime(inpFormat, inpDatetime string) (time.Time, error) {
	t, err := time.Parse(inpFormat, inpDatetime)
	if err != nil {
		return t, err
	}
	return t, nil
}
